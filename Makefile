#!/bin/sh

clean:
	rm -f bot/*.rice-box.go
	rm -f bot/rice-box.go

build: clean
	rm -f bot/*.rice-box.go
	go get github.com/GeertJohan/go.rice
	go get github.com/GeertJohan/go.rice/rice
	go build

dist: build
	mkdir -p dist
	cp bookfags-radio dist/
	cp -r templates dist/
	cp -r static dist/
	cp config.json.example dist/config.json


inlinedist: build
	mkdir -p inlinedist
	$(GOPATH)/bin/rice --import-path=bitbucket.org/kerrigan/bookfags-radio/bot embed-go
	cp bookfags-radio inlinedist/
	cp config.json.example inlinedist/
