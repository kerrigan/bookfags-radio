package bot

import "time"

type AirTimestamp struct {
	Timestamp int64  `json:"time"`
	Label     string `json:"label"`
}

type AirState struct {
	AirStarted    int64          `json:"started"`
	Mode          string         `json:"mode"`
	AirTimestamps []AirTimestamp `json:"timestamps"`
}

func (p *RadioStation) startAir() {
	StartAir(func() {
		p.airState = AIR_STARTED
		p.airStarted = time.Now().Unix()
		p.airRecorder.ClearTimestamps()

		p.notifyStartAir()

		data := WSPayload{
			Type: "airstate",
			Payload: AirState{
				AirStarted:    p.airStarted,
				Mode:          p.airState,
				AirTimestamps: p.airRecorder.airTimestamps,
			},
		}

		p.airRecorder.Start(p.config.AirRecordsPath)

		//TODO: run record

		p.adminWsBroadcast(data)
	})
}

func (p *RadioStation) startDj() {
	StartDJAir(func() {
		p.airState = DJ_AIR_STARTED
		p.airStarted = time.Now().Unix()

		p.notifyStartDJ()

		data := WSPayload{
			Type: "airstate",
			Payload: AirState{
				AirStarted: p.airStarted,
				Mode:       p.airState,
			},
		}

		//TODO: stop record
		p.airRecorder.Stop()
		//TODO: save tracking

		p.adminWsBroadcast(data)
	})
}

func (p *RadioStation) startMpd() {
	StopAir(func() {
		p.airState = MPD_STARTED
		p.airStarted = time.Now().Unix()

		p.notifyStopAir()

		data := WSPayload{
			Type: "airstate",
			Payload: AirState{
				AirStarted: p.airStarted,
				Mode:       p.airState,
			},
		}

		//TODO: stop record
		p.airRecorder.Stop()

		//TODO: save tracking

		p.adminWsBroadcast(data)
	})
}

func (p *RadioStation) notifyStartAir() {
	p.bot.WriteMessage(`<span style="background-color:#ff0000; color:#ffffff">&nbsp;&nbsp;MUMBLE ON AIR&nbsp;&nbsp;</span>`)
}

func (p *RadioStation) notifyStartDJ() {
	p.bot.WriteMessage(`<span style="background-color:#ff0000; color:#ffffff">&nbsp;&nbsp;DJ ON AIR&nbsp;&nbsp;</span>`)
}
func (p *RadioStation) notifyStopAir() {
	p.bot.WriteMessage(`<span style="background-color:green; color:#ffffff">&nbsp;&nbsp;MPD&nbsp;&nbsp;</span>`)
}
