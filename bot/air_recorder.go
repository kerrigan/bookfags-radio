package bot

import (
	"bufio"
	"encoding/json"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"sync"
	"time"
)

type RadioStreamSaver struct {
	client        *http.Client
	c             chan int
	mountURL      string
	started       bool
	airTimestamps []AirTimestamp
	lock          sync.Locker
}

func NewRadioStreamSaver(mountURL string) *RadioStreamSaver {
	return &RadioStreamSaver{
		client:        &http.Client{},
		c:             make(chan int),
		mountURL:      mountURL,
		started:       false,
		lock:          &sync.Mutex{},
		airTimestamps: []AirTimestamp{},
	}
}

func createOrOpenFile(path string) (recordOutputFile *os.File, trackingOutputFile *os.File, fileError error) {
	var f *os.File

	now := time.Now().Format("02-01-2006_15:04:05")

	filename := filepath.Join(path, now+".ogg")

	if _, err := os.Stat(filename); os.IsNotExist(err) {
		f, err = os.Create(filename)

		if err != nil {
			log.Println(err)
			fileError = err
			return
		}
	} else {
		f, err = os.OpenFile(filename, os.O_APPEND|os.O_WRONLY, 0666)

		if err != nil {
			log.Println(err)
			fileError = err
			return
		}
	}

	recordOutputFile = f

	f, err := os.Create(filepath.Join(path, now+".json"))

	if err != nil {
		fileError = err
		return
	}
	trackingOutputFile = f

	return
}

func (p *RadioStreamSaver) Start(path string) error {
	defer func() {
		p.lock.Unlock()
	}()

	p.lock.Lock()

	if p.started {
		return nil
	}

	p.started = true

	req, err := http.NewRequest("GET", p.mountURL, nil)
	if err != nil {
		log.Println(err)
		p.started = false
		return err
	}

	outputFile, trackingFile, err := createOrOpenFile(path)

	if err != nil {
		log.Println(err)
		p.started = false
		return err
	}

	resp, err := p.client.Do(req)

	if err != nil {
		log.Println(err)
		p.started = false
		return err
	}

	reader := bufio.NewReader(resp.Body)

	go p.readLoop(reader, resp.Body, outputFile, trackingFile)

	return nil
}

func (p *RadioStreamSaver) readLoop(reader io.Reader, body io.ReadCloser, writer io.WriteCloser, trackWriter io.WriteCloser) {
	defer func() {
		p.started = false
		writer.Close()
		body.Close()
	}()

	p.started = true
	buffer := make([]byte, 4096)
	for {
		select {
		case <-p.c:
			{
				log.Println("Recording finished")
				trackEncoder := json.NewEncoder(trackWriter)
				err := trackEncoder.Encode(p.airTimestamps)

				if err != nil {
					log.Println(err)
				}
				trackWriter.Close()

				return
			}

		default:
			{

				nread, err := reader.Read(buffer)
				if err != nil {
					log.Println(err)
					return
				}

				//log.Printf("Read %d bytes\n", nread)
				writer.Write(buffer[:nread])
			}
		}
	}

}

func (p *RadioStreamSaver) Stop() {
	defer func() {
		p.lock.Unlock()
	}()

	p.lock.Lock()
	if p.started {
		p.c <- 1
	}
}

func (p *RadioStreamSaver) ClearTimestamps() {
	p.airTimestamps = []AirTimestamp{
		AirTimestamp{
			Timestamp: 0,
			Label:     "Начало эфира",
		},
	}
}
