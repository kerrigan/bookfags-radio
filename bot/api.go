package bot

import (
	"net/http"

	"github.com/dchest/captcha"

	"bitbucket.org/kerrigan/bookfags-radio/utils"
)

func (p *RadioStation) APIAdminChatMessages(w http.ResponseWriter, r *http.Request) {

	data := struct {
		Messages []utils.LogMessage `json:"messages"`
		Users    []string           `json:"users"`
	}{
		p.logStorage.Messages,
		p.logStorage.Users,
	}
	toJson(w, data)
}

func (p *RadioStation) APIAdminChatNewMessage(w http.ResponseWriter, r *http.Request) {

	r.ParseForm()
	message := r.FormValue("message")

	if message == "" {
		return
	}

	p.logStorage.AddMessage("Администратор", message, false, "")
}

func (p *RadioStation) APIAdminAirState(w http.ResponseWriter, r *http.Request) {

	data := AirState{}

	if r.Method == "POST" {
		r.ParseForm()

		action := r.FormValue("action")

		switch action {
		case AIR_STARTED:
			p.startAir()
		case DJ_AIR_STARTED:
			p.startDj()
		case MPD_STARTED:
			p.startMpd()
		}
	} else if r.Method == "GET" {
		airState := p.airState
		data.Mode = airState
		data.AirStarted = p.airStarted
		data.AirTimestamps = p.airRecorder.airTimestamps

		toJson(w, data)
	}
}

func (p *RadioStation) APIAdminBanUser(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	user := r.PostFormValue("user")

	if user != "" {
		p.aclStorage.BanUser(user)
	}
}

func (p *RadioStation) APIAdminUnbanUser(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	user := r.PostFormValue("user")

	if user != "" {
		p.aclStorage.UnbanUser(user)
	}
}

func (p *RadioStation) APIAdminBanIP(w http.ResponseWriter, r *http.Request) {

	r.ParseForm()
	ip := r.PostFormValue("ip")

	if ip != "" {
		p.aclStorage.BanIP(ip)
	}
}

func (p *RadioStation) APIAdminUnbanIP(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	ip := r.PostFormValue("ip")

	if ip != "" {
		p.aclStorage.UnbanIP(ip)
	}
}

func (p *RadioStation) APIAdminBanned(w http.ResponseWriter, r *http.Request) {
	data := struct {
		Users []string `json:"users"`
		IPs   []string `json:"ips"`
	}{
		Users: p.aclStorage.GetBannedUsers(),
		IPs:   p.aclStorage.GetBannedIPs(),
	}

	toJson(w, data)
}

func (p *RadioStation) APIAdminLogin(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	username := r.PostFormValue("username")
	password := r.PostFormValue("password")

	if username == p.config.AdminLogin && password == p.config.AdminPassword {

		session, _ := p.sessionStore.Get(r, "session")

		session.Values["login"] = "admin"
		session.Save(r, w)

		data := struct {
			Success bool `json:"success"`
		}{
			Success: true,
		}

		toJson(w, data)
		return
	}

	data := struct {
		Success bool `json:"success"`
	}{
		Success: false,
	}

	toJson(w, data)
}

func (p *RadioStation) APIAdminLogout(w http.ResponseWriter, r *http.Request) {
	session, _ := p.sessionStore.Get(r, "session")

	delete(session.Values, "login")
	session.Save(r, w)
}

func (p *RadioStation) APIChatCaptcha(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()

	var captchaId string
	userNameCookie, err := r.Cookie("user")
	if err == nil {
		p.captchaTimeoutLock.Lock()
		if captchaFinished, ok := p.captchaTimeoutStore[userNameCookie.Value]; captchaFinished && ok {
			captchaId = ""
		} else {
			captchaId = captcha.New()
		}
		p.captchaTimeoutLock.Unlock()
	} else {
		captchaId = captcha.New()
	}

	data := struct {
		Captcha string `json:"captcha"`
	}{
		Captcha: captchaId,
	}

	toJson(w, data)
}
