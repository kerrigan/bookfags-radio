package bot

import (
	"encoding/json"
	"net/http"
)

func (p *RadioStation) APIAdminAirTracker(w http.ResponseWriter, r *http.Request) {
	//log.Println(r)
	r.ParseForm()
	switch r.Method {
	case http.MethodPost:
		//log.Println(r)

		var timestamps []AirTimestamp

		timestampsStr := r.PostFormValue("timestamps")
		err := json.Unmarshal([]byte(timestampsStr), &timestamps)

		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}

		p.airRecorder.airTimestamps = timestamps

		toJson(w, p.airState)
	default:
		toJson(w, map[string]bool{
			"ty": true,
		})
	}
}
