package bot

import (
	"net/http"
	"strconv"
	"strings"
)

func (p *RadioStation) APIAdminIcecastUsers(w http.ResponseWriter, r *http.Request) {
	toJson(w, p.iceUserStorage.Users)
}

func (p *RadioStation) APIAdminIcecastNewUser(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()

	login := r.FormValue("login")
	password := r.FormValue("password")

	if strings.Trim(login, " ") == "" || password == "" {
		return
	}
	p.iceUserStorage.AddUser(login, password)
}

func (p *RadioStation) APIAdminIcecastDeleteUser(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()

	indexStr := r.FormValue("index")

	if index, err := strconv.Atoi(indexStr); err == nil {
		p.iceUserStorage.RemoveUser(index)
	}
}

func (p *RadioStation) APIAdminIcecastEditUser(w http.ResponseWriter, r *http.Request) {
	indexStr := r.FormValue("index")

	index, err := strconv.Atoi(indexStr)

	if err != nil {
		return
	}

	if index < 0 && index > len(p.iceUserStorage.Users)-1 {
		return
	}

	login := r.FormValue("login")
	password := r.FormValue("password")
	blockedStr := r.FormValue("blocked")

	var blocked bool
	if blockedStr == "true" {
		blocked = true
	} else {
		blocked = false
	}

	if strings.Trim(login, " ") == "" || password == "" {
		return
	}

	p.iceUserStorage.EditUser(index, login, password, blocked)
}
