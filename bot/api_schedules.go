package bot

import (
	"net/http"
	"strconv"
	"strings"
	"time"
)

func (p *RadioStation) APIAdminScheduleNewAir(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()

	beginStr := r.FormValue("begin")
	endStr := r.FormValue("end")

	tsFormat := "02.01.2006 15:04:05"

	begin, err := time.Parse(tsFormat, beginStr)

	if err != nil {
		//TODO: write error
		toJson(w, struct {
			Status string `json:"status"`
			Error  string `json:"error"`
		}{
			"error",
			"Неправильный формат времени начала",
		})
		return
	}

	end, err := time.Parse(tsFormat, endStr)

	if err != nil {
		//TODO: write error
		toJson(w, struct {
			Status string `json:"status"`
			Error  string `json:"error"`
		}{
			"error",
			"Неправильный формат времени окончания",
		})
		return
	}

	title := r.FormValue("title")
	dj := r.FormValue("dj")

	if strings.Trim(title, " ") == "" || strings.Trim(dj, " ") == " " {
		//TODO: write error
		toJson(w, struct {
			Status string `json:"status"`
			Error  string `json:"error"`
		}{
			"error",
			"Не введены заголовок эфира или диджей",
		})
		return
	}

	p.scheduleStorage.AddSchedule(begin, end, title, dj)

	toJson(w, struct {
		Status string `json:"status"`
	}{
		"ok",
	})
}

func (p *RadioStation) APIAdminScheduleDeleteAir(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()

	indexStr := r.FormValue("index")

	if index, err := strconv.Atoi(indexStr); err == nil {
		p.scheduleStorage.RemoveSchedule(index)
	}
}

func (p *RadioStation) APIAdminScheduleEditAir(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()

	indexStr := r.FormValue("index")

	index, err := strconv.Atoi(indexStr)

	if err != nil || index < 0 {
		toJson(w, struct {
			Status string `json:"status"`
			Error  string `json:"error"`
		}{
			"error",
			"Эфир не существует",
		})
		return
	}

	beginStr := r.FormValue("begin")
	endStr := r.FormValue("end")

	tsFormat := "02.01.2006 15:04:05"

	begin, err := time.Parse(tsFormat, beginStr)

	if err != nil {
		//TODO: write error
		toJson(w, struct {
			Status string `json:"status"`
			Error  string `json:"error"`
		}{
			"error",
			"Неправильный формат времени начала",
		})
		return
	}

	end, err := time.Parse(tsFormat, endStr)

	if err != nil {
		//TODO: write error
		toJson(w, struct {
			Status string `json:"status"`
			Error  string `json:"error"`
		}{
			"error",
			"Неправильный формат времени окончания",
		})
		return
	}

	title := r.FormValue("title")
	dj := r.FormValue("dj")

	if strings.Trim(title, " ") == "" || strings.Trim(dj, " ") == " " {
		//TODO: write error
		toJson(w, struct {
			Status string `json:"status"`
			Error  string `json:"error"`
		}{
			"error",
			"Не введены заголовок эфира или диджей",
		})
		return
	}

	p.scheduleStorage.EditSchedule(index, begin, end, title, dj)

	toJson(w, struct {
		Status string `json:"status"`
	}{
		"ok",
	})
}
