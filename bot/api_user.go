package bot

import (
	"fmt"
	"net/http"
	"regexp"
	"strings"
	"time"

	"bitbucket.org/kerrigan/bookfags-radio/utils"
	"github.com/dchest/captcha"
)

const CHAT_MESSAGE_MAX_LENGTH = 1000

func (p *RadioStation) APIChatMessages(w http.ResponseWriter, r *http.Request) {
	oldMessages := p.logStorage.Messages

	messages := make([]utils.LogMessage, 0)

	for _, message := range oldMessages {
		if message.IsHearer {
			message.Nick = ""
		}
		message.IP = ""
		messages = append(messages, message)
	}

	data := struct {
		Messages []utils.LogMessage `json:"messages"`
		Users    []string           `json:"users"`
	}{
		messages,
		p.logStorage.Users,
	}
	toJson(w, data)
}

func (p *RadioStation) APIChatNewMessage(w http.ResponseWriter, r *http.Request) {

	//TODO: write logic and other, take from FeedBackHandler
	r.ParseForm()
	message := r.FormValue("message")
	captchaId := r.FormValue("captchaKey")
	captchaString := r.FormValue("captcha")

	userNameCookie, err := r.Cookie("user")

	cookieExpire := time.Now().AddDate(1, 0, 0)

	if err != nil {
		userNameCookie = &http.Cookie{Name: "user", Value: randomNick(), Path: "/", Expires: cookieExpire}
		http.SetCookie(w, userNameCookie)
	}

	if matched, _ := regexp.MatchString("[a-z]{6}[0-9]{1,2}",
		userNameCookie.Value); !matched {
		userNameCookie = &http.Cookie{Name: "user", Value: randomNick(), Path: "/", Expires: cookieExpire}
		http.SetCookie(w, userNameCookie)
	}

	userName := userNameCookie.Value
	realIP := getRealIP(r)

	if p.checkBanned(userName, realIP) {
		http.Error(w, "U R BANNED", http.StatusForbidden)
		return
	}

	if strings.Trim(message, " ") == "" {
		return
	}

	captchaError := ""
	if r.Method == "POST" {

		p.captchaTimeoutLock.Lock()

		if len(message) > CHAT_MESSAGE_MAX_LENGTH {
			captchaError = fmt.Sprintf(`Сообщение длиннее %d символов`, CHAT_MESSAGE_MAX_LENGTH)
		} else if captchaFinished, ok := p.captchaTimeoutStore[userName]; captchaFinished && ok {
			p.logStorage.AddHearerMessage(userName, message, realIP)
		} else if !captcha.VerifyString(captchaId, captchaString) {
			captchaError = `Неправильная капча`
		} else  {
			//fmt.Println(message)
			p.captchaTimeoutStore[userName] = true
			p.logStorage.AddHearerMessage(userName, message, realIP)
		}
		p.captchaTimeoutLock.Unlock()

		var data interface{}
		if len(captchaError) > 0 {
			data = struct {
				Result string `json:"result"`
				Error  string `json:"error"`
			}{
				Result: "error",
				Error:  captchaError,
			}
		} else {
			data = struct {
				Result string `json:"result"`
			}{
				"ok",
			}
		}
		toJson(w, data)
	}

	//p.logStorage.AddMessage("Администратор", message, false, "")
}

func (p *RadioStation) APISchedule(w http.ResponseWriter, r *http.Request) {
	toJson(w, p.scheduleStorage.GetSchedules())
}
