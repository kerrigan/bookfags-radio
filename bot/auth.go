package bot

import (
	"net/http"
	"sort"
)

func (p *RadioStation) L(w http.ResponseWriter, r *http.Request) bool {
	session, _ := p.sessionStore.Get(r, "session")

	if loggedIn, ok := session.Values["login"]; ok {
		if loggedIn == "admin" {
			return true
		}
	}

	http.Error(w, "Unauthorized", http.StatusForbidden)
	return false
}

func (p *RadioStation) LM(handlerFunc func(w http.ResponseWriter, r *http.Request)) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		session, _ := p.sessionStore.Get(r, "session")

		if loggedIn, ok := session.Values["login"]; ok {
			if loggedIn == "admin" {
				handlerFunc(w, r)
				return
			}
		}

		http.Error(w, "Unauthorized", http.StatusForbidden)
	})
}

func (p *RadioStation) InWhiteList(mumbleNick string) bool {
	if sort.SearchStrings(p.config.WhiteList, mumbleNick) < len(p.config.WhiteList) {
		return true
	}
	return false
}

func (p *RadioStation) InLogBlackList(mumbleNick string) bool {
	if sort.SearchStrings(p.config.LogBlackList, mumbleNick) < len(p.config.LogBlackList) {
		return true
	}

	return false
}
