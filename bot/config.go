package bot

import (
	"encoding/json"
	"log"
	"os"
)

const (
	AIR_STARTED    = "air"
	DJ_AIR_STARTED = "dj"
	MPD_STARTED    = "mpd"
)

type Config struct {
	IcecastHost     string   `json:"icecastHost"`
	IcecastMount    string   `json:"icecastMount"`
	IcecastPassword string   `json:"icecastPassword"`
	IcecastPort     int      `json:"icecastPort"`
	IcecastUser     string   `json:"icecastUser"`
	MumbleHost      string   `json:"mumbleHost"`
	MumblePort      int      `json:"mumblePort"`
	Nick            string   `json:"nick"`
	WebPort         int      `json:"webPort"`
	SecretSalt      string   `json:"secretSalt"`
	AdminLogin      string   `json:"adminLogin"`
	AdminPassword   string   `json:"adminPassword"`
	AudioStreamUrl  string   `json:"audioStreamUrl"`
	TwitterWidgetId string   `json:"twitterWidgetId"`
	SiteTitle       string   `json:"siteTitle"`
	WhiteList       []string `json:"whiteList"`
	LogBlackList    []string `json:"logBlackList"`
	AirRecordsPath  string   `json:"airRecordsPath"`
}

func ParseConfig(filename string) (config Config, err error) {
	file, err := os.Open(filename)

	if err != nil {
		log.Println("Failed to open config")
		return
	}
	decoder := json.NewDecoder(file)
	err = decoder.Decode(&config)

	if err != nil {
		log.Println("Failed to parse config")
		return
	}

	return config, nil
}
