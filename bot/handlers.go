package bot

import (
	"log"
	"net/http"
)

func (p *RadioStation) APIIcecastAuth(w http.ResponseWriter, r *http.Request) {
	log.Println(r)
	r.ParseForm()

	action := r.PostFormValue("action")
	/*
		mount := r.PostFormValue("mount")
			server := r.PostFormValue("server")
			port := r.PostFormValue("port")
	*/
	username := r.PostFormValue("user")
	password := r.PostFormValue("pass")
	/*
		clientID := r.PostFormValue("client")
		IP := r.PostFormValue("ip")
		userAgent := r.PostFormValue("agent")
	*/

	//log.Println(action, mount, server, port, username, strings.Repeat("*", len(password)), clientID, IP, userAgent)

	if action != "stream_auth" {
		http.Error(w, "Not allowed", http.StatusBadRequest)
		return
	}

	if p.iceUserStorage.CheckUser(username, password) {
		w.Header().Set("icecast-auth-user", "1")
		//w.Write([]byte("icecast-auth-user: 1\r\n"))
		w.Write([]byte("OK"))
	} else {
		http.Error(w, "Not allowed", http.StatusForbidden)
		return
	}
}
