package bot

import (
	"encoding/json"
	"fmt"
	"net/http"

	"bitbucket.org/kerrigan/gomumbot/mumbot"
)

type IcecastResponse struct {
	Icestats IceStats `json:"icestats"`
}

/*
type IceStats struct {
	Sources []Source `json:"source"`
}
*/
type IceStats struct {
	Sources Source `json:"source"`
}

type Source struct {
	Listeners    int    `json:"listeners"`
	ListenersMax int    `json:"listener_peak"`
	Title        string `json:"title"`
	Artist       string `json:"artist"`
	Url          string `json:"listenurl"`
}

func parseStat(v Source) string {
	return fmt.Sprintf("%d/%d %s<br>", v.Listeners, v.ListenersMax, v.Url)
}

func parseStats(url string) string {
	msg := ""

	resp, err := http.Get(url)
	if err != nil {
		msg += "Релей недоступен<br>"
	} else {
		var response IcecastResponse
		err := json.NewDecoder(resp.Body).Decode(&response)
		if err != nil {
			msg += "Релей недоступен<br>"
		} else {
			/*
				for _, v := range response.Icestats.Sources {
					msg += fmt.Sprintf("%d/%d %s<br>", v.Listeners, v.ListenersMax, v.Url)
				}
			*/
			msg += parseStat(response.Icestats.Sources)
		}
	}
	return msg
}

func (p *RadioStation) getIcecastsStats(bot *mumbot.MumbleBot, user *mumbot.User) {
	msg := "<br>" + parseStats("http://radio.bookfags.ru:8000/status-json.xsl")

	p.lock.Lock()
	bot.WritePrivateMessage(user.ID, msg)
	p.lock.Unlock()
}
