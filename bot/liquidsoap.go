package bot

import (
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"os"
	"os/exec"
	"strconv"
	"syscall"
)

func (p *RadioStation) buildLiquidsoapConfig() {
	configText :=
		`
  host=` + strconv.Quote(p.config.IcecastHost) + `
  username=` + strconv.Quote(p.config.IcecastUser) + `
  password = ` + strconv.Quote(p.config.IcecastPassword) + `
  mount = ` + strconv.Quote(p.config.IcecastMount) + `
  port = ` + strconv.Itoa(p.config.IcecastPort) + ``

	ioutil.WriteFile("data/authdata.liq", []byte(configText), 0644)
}

func (p *RadioStation) runLiquidSoap() {
	p.buildLiquidsoapConfig()
	p.lsProcess = exec.Command("liquidsoap", "-v", "mpdmixer.liq")

	p.lsProcess.Stdout = os.Stdout
	p.lsProcess.Stderr = os.Stderr

	err := p.lsProcess.Start()

	if err != nil {
		log.Fatal(err)
	}

	ready := false
	for !ready {
		_, err := http.Get("http://localhost:8006/")
		if err == nil {
			ready = true
		}
	}

}

func (p *RadioStation) stopLiquidSoap() {
	if p.lsProcess != nil {
		p.lsProcess.Process.Signal(syscall.SIGINT)
	}
}

func sendSoapCmd(cmd string) (err error) {
	conn, err := net.Dial("tcp", "localhost:9999")
	if err != nil {
		log.Println(err)
		return err
	}
	defer conn.Close()
	fmt.Fprintf(conn, cmd)
	return nil
}

func StartAir(callback func()) {
	sendSoapCmd("air.start_air\r\n")
	callback()
}

func StartDJAir(callback func()) {
	sendSoapCmd("air.start_dj\r\n")
	callback()
}

func StopAir(callback func()) {
	sendSoapCmd("air.stop_any\r\n")
	callback()
}
