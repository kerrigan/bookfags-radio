package bot

import (
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"

	rice "github.com/GeertJohan/go.rice"
	"github.com/dchest/captcha"
	"github.com/gorilla/mux"
)

func (p *RadioStation) RunHttp(httpPort int) {
	/*
			db, err := sql.Open("sqlite3", "./db.sqlite")
			defer db.Close()

		if err != nil {
			log.Fatal(err)
		}
	*/

	r := mux.NewRouter()
	r.HandleFunc("/config.js", func(w http.ResponseWriter, r *http.Request) {
		js := `app.factory('settings', [function(){
			service = {};
			service.audioStreamUrl = "` + p.config.AudioStreamUrl + `";
			service.twitterWidgetId = "` + p.config.TwitterWidgetId + `";
			service.title = "` + p.config.SiteTitle + `";
			service.enableStreaming = false;
			return service;
		}]);`

		w.Write([]byte(js))
	})
	r.Handle("/captcha/{id}.png", captcha.Server(captcha.StdWidth, captcha.StdHeight))
	//r.HandleFunc("/feedback", p.FeedBackHandler)

	templateBox := rice.MustFindBox("../templates")
	r.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		//http.ServeFile(w, r, "templates/index.html")
		w.Write(templateBox.MustBytes("index.html"))
	})

	r.HandleFunc("/info.html", func(w http.ResponseWriter, r *http.Request) {
		data, err := ioutil.ReadFile("data/info.html")
		if err != nil {
			return
		}
		w.Write(data)
	})
	r.HandleFunc("/login", func(w http.ResponseWriter, r *http.Request) {
		//http.ServeFile(w, r, "templates/login.html")
		w.Write(templateBox.MustBytes("login.html"))
	})
	//r.HandleFunc("/admin", p.AdminHandler)

	adminRouter := r.PathPrefix("/admin").Subrouter()

	adminRouter.Handle("/", p.LM(func(w http.ResponseWriter, r *http.Request) {
		//http.ServeFile(w, r, "templates/admin.html")
		w.Write(templateBox.MustBytes("admin.html"))
	}))

	adminRouter.Handle("/control", p.LM(p.APIAdminAirState))
	adminRouter.Handle("/airtracker", p.LM(p.APIAdminAirTracker))
	adminRouter.Handle("/ws", p.LM(p.AdminWsHandler))
	adminRouter.Handle("/chat/messages/new", p.LM(p.APIAdminChatNewMessage))
	adminRouter.Handle("/chat/messages", p.LM(p.APIAdminChatMessages))

	r.HandleFunc("/ws", p.WsHandler)
	apiRouter := r.PathPrefix("/api").Subrouter()

	apiRouter.Handle("/chat/ban", p.LM(p.APIAdminBanUser))
	apiRouter.Handle("/chat/unban", p.LM(p.APIAdminUnbanUser))
	apiRouter.Handle("/chat/banip", p.LM(p.APIAdminBanIP))
	apiRouter.Handle("/chat/unbanip", p.LM(p.APIAdminUnbanIP))
	apiRouter.Handle("/chat/banned", p.LM(p.APIAdminBanned))

	apiRouter.HandleFunc("/chat/captcha", p.APIChatCaptcha)
	apiRouter.HandleFunc("/chat/messages", p.APIChatMessages)

	apiRouter.HandleFunc("/schedule", p.APISchedule)
	apiRouter.Handle("/schedule/new", p.LM(p.APIAdminScheduleNewAir))
	apiRouter.Handle("/schedule/edit", p.LM(p.APIAdminScheduleEditAir))
	apiRouter.Handle("/schedule/delete", p.LM(p.APIAdminScheduleDeleteAir))

	apiRouter.HandleFunc("/chat/messages/new", p.APIChatNewMessage)
	apiRouter.HandleFunc("/login", p.APIAdminLogin)
	apiRouter.HandleFunc("/logout", p.APIAdminLogout)

	apiRouter.HandleFunc("/iceauth", p.APIIcecastAuth)

	apiRouter.Handle("/iceuser/new", p.LM(p.APIAdminIcecastNewUser))
	apiRouter.Handle("/iceuser/edit", p.LM(p.APIAdminIcecastEditUser))
	apiRouter.Handle("/iceuser/delete", p.LM(p.APIAdminIcecastDeleteUser))
	apiRouter.Handle("/iceuser", p.LM(p.APIAdminIcecastUsers))

	staticBox := rice.MustFindBox("../static")
	r.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(staticBox.HTTPBox())))
	r.PathPrefix("/templates/").Handler(http.StripPrefix("/templates/", http.FileServer(templateBox.HTTPBox())))
	//r.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("./static/"))))

	go p.wsWriteLoop(
		p.logStorage.MessageBus,
		p.logStorage.UsersBus,
		p.aclStorage.UsersBus,
		p.aclStorage.IPsBus,
		p.scheduleStorage.ScheduleBus,
		p.iceUserStorage.UsersBus,
	)

	l, err := net.Listen("tcp4", fmt.Sprintf(":%v", httpPort))
	if err != nil {
		log.Fatal(err)
	}
	if err := http.Serve(l, r); err != nil {
		log.Fatal(err)
	}
}
