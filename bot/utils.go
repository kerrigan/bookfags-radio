package bot

import (
	"encoding/json"
	"math/rand"
	"net/http"
	"strconv"
	"strings"
)

func randomNick() string {
	letters := []rune("abcdefghijklmnopqrstuvwxyz")
	b := make([]rune, 6)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	result := string(b)

	result += strconv.Itoa(rand.Intn(100))

	return result
}

func getRealIP(r *http.Request) string {
	if ip, ok := r.Header["X-Forwarded-For"]; ok {
		return ip[0]
	} else {
		return strings.Split(r.RemoteAddr, ":")[0]
	}
}

func (p *RadioStation) checkBanned(userName, IP string) bool {
	return p.aclStorage.IsBanned(userName, IP)
}

func check500(err error, w http.ResponseWriter) {
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func toJson(w http.ResponseWriter, data interface{}) {
	raw, err := json.Marshal(data)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Write(raw)
}
