package bot

import (
	"html"
	"log"
	"net/http"

	"bitbucket.org/kerrigan/bookfags-radio/utils"
	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

func (p *RadioStation) wsBroadcast(message interface{}) {
	for _, conn := range p.wsConnections {
		conn.WriteJSON(message)
	}
}

func (p *RadioStation) adminWsBroadcast(message interface{}) {
	for _, conn := range p.adminWsConnections {
		conn.WriteJSON(message)
	}
}

func (p *RadioStation) wsWriteLoop(
	msgChan chan utils.LogMessage,
	usersChan chan []string,
	aclUsersChan chan []string,
	aclIPsChan chan []string,
	scheduleChan chan []utils.Schedule,
	iceUsersChan chan utils.IcecastUsers,
) {
	for {
		msg := WSPayload{}
		select {
		case message := <-msgChan:
			msg.Type = "message"
			if message.IsHearer {
				message.Text = html.EscapeString(message.Text)
			}

			msg.Payload = message
			p.adminWsBroadcast(msg)

			if message.IsHearer {
				message.Nick = "Слушатель"
				message.IP = ""
			}
			msg.Payload = message
			p.wsBroadcast(msg)

		case users := <-usersChan:
			msg.Type = "users"
			msg.Payload = users
			p.adminWsBroadcast(msg)
			p.wsBroadcast(msg)

			//Only admin data
		case bannedUsers := <-aclUsersChan:
			msg.Type = "bannedusers"
			msg.Payload = bannedUsers
			p.adminWsBroadcast(msg)

		case bannedIPs := <-aclIPsChan:
			msg.Type = "bannedips"
			msg.Payload = bannedIPs
			p.adminWsBroadcast(msg)
		case schedule := <-scheduleChan:
			msg.Type = "schedule"
			msg.Payload = schedule
			p.adminWsBroadcast(msg)
			p.wsBroadcast(msg)

		case iceusers := <-iceUsersChan:
			msg.Type = "iceusers"
			msg.Payload = iceusers
			p.adminWsBroadcast(msg)
		}

	}
}

func (p *RadioStation) wsReadLoop(conn *websocket.Conn) {
	for {
		if _, _, err := conn.NextReader(); err != nil {
			conn.Close()
			delete(p.wsConnections, conn.RemoteAddr())
			//fmt.Println(connections)
			break
		}
	}
}

type WSPayload struct {
	Type    string      `json:"type"`
	Payload interface{} `json:"data"`
}

func (p *RadioStation) WsHandler(w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)

	if err != nil {
		log.Println(err)
		return
	}
	p.wsConnections[conn.RemoteAddr()] = conn
	//fmt.Println(connections)
	go p.wsReadLoop(conn)
}

func (p *RadioStation) adminWsReadLoop(conn *websocket.Conn) {
	for {
		if _, _, err := conn.NextReader(); err != nil {
			conn.Close()
			delete(p.wsConnections, conn.RemoteAddr())
			//fmt.Println(connections)
			break
		}
	}
}

func (p *RadioStation) AdminWsHandler(w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)

	if err != nil {
		log.Println(err)
		return
	}
	p.adminWsConnections[conn.RemoteAddr()] = conn
	//fmt.Println(connections)
	go p.adminWsReadLoop(conn)
}
