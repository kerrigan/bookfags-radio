package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"

	"bitbucket.org/kerrigan/bookfags-radio/bot"
)

func main() {

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	signal.Notify(c, syscall.SIGTERM)

	var configFileName = flag.String("c", "config.json", "config file name")
	flag.Parse()

	config, err := bot.ParseConfig(*configFileName)

	if err != nil {
		log.Fatal("Can't load config file")
	}

	radio := bot.NewRadioStation(&config)

	go func() {
		for _ = range c {
			radio.Stop()
			fmt.Println("Stopping mumble bot.")
			os.Exit(0)
		}
	}()

	radio.Run()
}
