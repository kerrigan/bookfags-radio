
app.controller('StreamController', ['$scope', '$http', '$interval', 'settings',
 function($scope, $http, $interval, settings){
   $scope.running = false;

   $scope.airStarted = 0;
   $scope.airTimeElapsed = 0;

   $scope.restricted = settings.enableStreaming;
   if(!settings.enableStreaming){
       return;
   }
   /*
   var airStateTimer;
   airStateTimer = $interval(function(){
     $http.get("/admin/control")
     .success(function(data){
       $scope.running = data.running;
       $scope.airStarted = data.started;
     })
   }, 5000);
   */


  $scope.startStream = function(){
    $http.post("/admin/control", {action: 'start'})
    .success(function(data){
      $scope.airStarted = data.started;
      $scope.running = data.running;
    });
  };

  $scope.stopStream = function(){
    $http.post("/admin/control", {action: 'stop'})
    .success(function(data){
      $scope.running = data.running;
    });
  };


  var timer;

  $scope.$watch('running', function(newVal, oldVal){
      if(newVal){
        timer = $interval(function(){
            $scope.airTimeElapsed = Math.floor(new Date().getTime() / 1000) - $scope.airStarted;
        }, 1000);
    } else {
        $interval.cancel(timer);
    }
  });

  //TODO: Air timer when air started
}]);

app.controller('LoginController', ['$scope', '$http',
 function($scope, $http){
  $scope.username = "";
  $scope.password = "";

  $scope.login = function(){
    $http.post("/api/login", {username: $scope.username,
                              password: $scope.password})
          .success(function(data, status, headers, config){
            if (data.success === true) {
                window.location = "/admin/";
            } else {
                alert("Неправильные логин/пароль");
            }
          })
          .error(function(data){
            alert("Невозможно произвести вход");
          });
  };
}]);


app.controller('NavBarController', ['$scope', '$http', 'settings',
    function($scope, $http, settings){

        $scope.logout = function(){
            $http.post("/api/logout")
            .success(function(data, status, headers, config){
                window.location = "/login";
            });
        };

        $scope.title = settings.title;

}])
