app.directive('aircontrol', function(){
  return {
    restrict: 'E',
    replace: true,
    templateUrl: '/templates/aircontrol.html',
    controller: ['$scope', '$http', '$interval', 'AdminWsService', function($scope, $http, $interval, AdminWsService) {
      $scope.airStarted = 0;
      $scope.airTimeElapsed = 0;
      $scope.mode = 'mpd';

      $http.get("/admin/control")
      .success(function(data){
        $scope.mode = data.mode;
        $scope.airStarted = data.started;
      });


      AdminWsService.register(function(data){
        if (data.type === "airstate") {
          var data = data.data;
          $scope.airStarted = data.started;
          $scope.mode = data.mode;
        }
      });


     $scope.startStream = function(){
       $http.post("/admin/control", {action: 'air'})
       .success(function(data){
       });
     };


     $scope.startDJStream = function() {
       $http.post("/admin/control", {action: 'dj'})
       .success(function(data){
       });
     }

     $scope.startMPD = function(){
       $http.post("/admin/control", {action: 'mpd'})
       .success(function(data){
       });
     };




     var timer;

     $scope.$watch('mode', function(newVal, oldVal){
         if(newVal != oldVal){
           if (newVal === "air" || newVal === "dj") {
             $interval.cancel(timer);

             timer = $interval(function(){
               var now = Math.floor(Date.now() / 1000);
               $scope.airTimeElapsed = now - $scope.airStarted;
             }, 1000);
           } else {
             $interval.cancel(timer);
             $scope.airTimeElapsed = 0;
           }
       }
     });
    }]
  }
});

app.directive('adminchat', function(){
  return {
    restrict: 'E',
    replace: true,
    templateUrl: '/templates/adminchat.html',
    controller: ['$scope', '$http', '$interval', 'AdminWsService', function($scope, $http, $interval, AdminWsService){
        $scope.messages = [];
        $scope.users = [];
        $scope.bannedUsers = [];
        $scope.bannedIPs = [];


        $scope.safeApply = function(fn) {
          var phase = this.$root.$$phase;
          if(phase == '$apply' || phase == '$digest') {
            if(fn && (typeof(fn) === 'function')) {
              fn();
            }
          } else {
            this.$apply(fn);
          }
        };

        AdminWsService.register(function(data){
            if(data.type === "message"){
                $scope.messages.push(data.data);
            } else if(data.type === "users"){
                $scope.users = data.data;
            } else if(data.type === "bannedusers"){
                $scope.bannedUsers = data.data;
            } else if(data.type === "bannedips"){
                $scope.bannedIPs = data.data;
            }
        });

        $scope.banUser = function(user){
            $http.post("/api/chat/ban", {user: user})
            .success(function(data){
              console.log("banned");
            });
        };

        $scope.banIP = function(ip){
          $http.post("/api/chat/banip", {ip: ip})
          .success(function(data){
              console.log("ip banned");
          })
        };

        $scope.unbanUser = function(user){
            $http.post("/api/chat/unban", {user: user})
            .success(function(data){
              console.log("unbanned");
            });
        };

        $scope.unbanIP = function(ip){
          $http.post("/api/chat/unbanip", {ip: ip})
          .success(function(data){
              console.log("ip unbanned");
          })
        };

        $scope.loadChatMessages = function(){
            $http.get("/admin/chat/messages")
            .success(function(data, status, headers, config) {
               // this callback will be called asynchronously
               // when the response is available
               console.log(data);
               $scope.messages = data.messages;
               $scope.users = data.users;
             })
             .error(function(data, status, headers, config) {
               // called asynchronously if an error occurs
               // or server returns response with an error status.
               console.log(data);
             });
        };

        $scope.loadBanned = function(){
          $http.get("/api/chat/banned")
          .success(function(data){
            $scope.bannedUsers = data.users;
            $scope.bannedIPs = data.ips;
          })
        };

        $scope.message = "";


        $scope.sendMessage = function(){
          $http.post("/admin/chat/messages/new",
                    {message: $scope.message})
          .success(function(data, status, headers, config){
            console.log(data);
            $scope.message = "";
          })
          .error(function(data, status, headers, config){
            console.log(data);
          });
        };

        $scope.loadChatMessages();
        $scope.loadBanned();
    }]
  }
});

app.directive('adminschedule', function(){
  return {
    restrict: 'E',
    replace: true,
    scope: {},
    templateUrl: '/templates/adminschedule.html',
    controller: ['$scope', '$http', 'AdminWsService', function($scope, $http, AdminWsService){
        $scope.schedules = [];
        $scope.form = {
          begin: "",
          end: "",
          title: "",
          dj: "",
          index: "-1"
        };

        AdminWsService.register(function(data){
            if(data.type === "schedule"){
                $scope.schedules = data.data;
            }
        });

        $scope.loadSchedules = function(){
          $http.get("/api/schedule")
          .then(function(data){
            $scope.schedules = data.data;
          });
        };

        $scope.addSchedule = function(){
          console.log("add schedule");
          $http.post("/api/schedule/new", $scope.form)
          .success(function(data, status, headers, config){
            if(data.status !== "ok"){
              alert(data.error);
            }
          });
        };

        $scope.loadSchedule = function(index, schedule){
            $scope.form = {
                index: index,
                begin: schedule.beginDate + " " + schedule.beginTime,
                end: schedule.endDate + " " + schedule.endTime,
                title: schedule.title,
                dj: schedule.dj
            };
        };

        $scope.removeSchedule = function(index){
          $http.post("/api/schedule/delete", {index: index})
          .success(function(data, status, headers, config){
            console.log("Saved");
          })
          .error(function(data, status, headers, config){
            console.log(data);
          });
        };

        $scope.saveSchedule = function(){
          $http.post("/api/schedule/edit", $scope.form)
          .success(function(data, status, headers, config){
            console.log("Saved");
            if(data.status !== "ok"){
              alert(data.error);
            }
          })
          .error(function(data, status, headers, config){
            console.log(data);
          });
        };

        $scope.loadSchedules();
    }]
  }
});


app.directive('icecastusers', function(){
  return {
    restrict: 'E',
    replace: true,
    templateUrl: '/templates/adminicecastusers.html',
    scope: {},
    controller: ['$scope', '$http', 'AdminWsService', function($scope, $http, AdminWsService){
      $scope.users = [];
      $scope.form = {
        login: "",
        password: "",
        banned: false,
        index: -1
      };

      AdminWsService.register(function(data){
          if(data.type === "iceusers"){
              $scope.users = data.data;
          }
      });

      $scope.loadUsers = function(){
        $http.get("/api/iceuser")
        .then(function(data){
          $scope.users = data.data;
        });
      };

      $scope.loadUser = function(index, user){
        $scope.form = {
          login: user.login,
          password: user.password,
          blocked: user.blocked,
          index: index
        };
      };

      $scope.addUser = function(){
        $http.post("/api/iceuser/new", $scope.form);
      };

      $scope.removeUser = function(index){
        $http.post("/api/iceuser/delete", {index: index});
      };

      $scope.saveUser = function(){
        $http.post("/api/iceuser/edit", $scope.form);
      };

      $scope.toggleBan = function(index, user){
        user.blocked = !user.blocked;
        user.index = index;
        $http.post("/api/iceuser/edit", user);
      }

      $scope.loadUsers();
    }]
  }
});


app.directive('airtimetracker', function(){
  return {
    restrict: 'E',
    replace: true,
    templateUrl: '/templates/airtimetracker.html',
    scope: {},
    controller: ['$scope', '$http', function($scope, $http){
      $scope.timestamps = [];

      $scope.airStarted = 0;

      $http.get("/admin/control")
      .success(function(data){
        $scope.airStarted = data.started;
        $scope.timestamps = data.timestamps;
      });


      $scope.saveLabel = function(index, timestamp) {
        $scope.timestamps[index].edit = false;
        console.log("Timestamp save");
        $http.post("/admin/airtracker", {"timestamps": angular.toJson($scope.timestamps)})
          .success(function(data){
            console.log("SENT");
          })
          .error(function(error){
            console.log(error);
          });

      }
      $scope.trackTime = function(e) {
        var now = Math.floor(new Date().getTime() / 1000);
        $scope.timestamps.push({"time": now - $scope.airStarted, "label": "", edit: true});
      }
    }]
  }
})
