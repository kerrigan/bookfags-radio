app.directive('radioplayer', function(){
  return {
    restrict: 'E',
    replace: true,
    templateUrl: '/templates/player.html',
    scope: {},
    controller: ['$scope', '$http', '$interval', 'audio', 'settings', function($scope, $http, $interval, audio, settings){
      var audio = audio;

      $scope.started = false;


      var prevTime;
      var checkIsAlive = function(){
        if (audio.currentTime <= prevTime) {
          console.log("Restart player");
          audio.pause();
          audio.currentTime = 0;
          prevTime = 0;
          audio.src = settings.audioStreamUrl;
          audio.play();
        }
        prevTime = audio.currentTime;
      };

      var stopChecker;

      $scope.play = function(){
        audio.src = settings.audioStreamUrl;
        audio.play();
        prevTime = audio.currentTime;
        stopChecker = $interval(checkIsAlive, 3 * 1000);
        $scope.started  = true;
      };

      $scope.stop = function(){
        audio.pause();
        $interval.cancel(stopChecker);
        $scope.started  = false;
      };

      $scope.mute = function(){
        audio.volume = 0;
      };

      $scope.setsnd = function(vol){
        audio.volume = vol;
      };
    }]
  }
});

app.directive('schedule', function(){
  return {
    restrict: 'E',
    replace: true,
    templateUrl: '/templates/schedule.html',
    scope: {},
    controller: ['$scope', '$http', 'UserWsService', function($scope, $http, UserWsService){
        $scope.schedule = [];

        UserWsService.register(function(data){
            if(data.type === "schedule"){
                $scope.schedules = data.data;
            }
        });

        $scope.loadSchedule = function(){
          $http.get("/api/schedule")
          .then(function(data){
            $scope.schedules = data.data;
          });
        };

        $scope.loadSchedule();
    }]
  }
});

app.directive('chat', function(){
  return {
    restrict: 'E',
    replace: true,
    templateUrl: '/templates/chat.html',
    scope: {},
    controller: ['$scope', '$http', '$interval', 'UserWsService', function($scope, $http, $interval, UserWsService){
      $scope.messages = [];
      $scope.users = [];
      $scope.result_timer = null;
      $scope.chatForm = {
          captchaKey: "",
          captcha: "",
          message: ""
      };

      UserWsService.register(function(data){
          if(data.type === "message"){
              $scope.messages.push(data.data);
          } else if(data.type === "users"){
              $scope.users = data.data;
          }
      });

      $scope.loadChatMessages = function(){
          $http.get("/api/chat/messages")
          .success(function(data, status, headers, config) {
             // this callback will be called asynchronously
             // when the response is available
             console.log(data);
             $scope.messages = data.messages;
             $scope.users = data.users;
           })
           .error(function(data, status, headers, config) {
             // called asynchronously if an error occurs
             // or server returns response with an error status.
             console.log(data);
           });
      };

      $scope.loadCaptcha = function(){
        $http.get("/api/chat/captcha")
        .success(function(data){
            $scope.chatForm.captchaKey = data.captcha;
            $scope.chatForm.captcha = "";
        });
      };

      $scope.sendMessage = function(){
        $http.post("/api/chat/messages/new", $scope.chatForm)
        .success(function(data, status, headers, config){
          if(data.result !== "ok"){
            $scope.infomessages = data.error;
	          if($scope.result_timer){
              clearTimeout($scope.result_timer);
            }
            $scope.result_timer = setTimeout(function() {
              $scope.result_timer = null;
              $scope.infomessages = '';
            },3000);
            return;
          }
          console.log(data);
          $scope.chatForm.message = "";
          $scope.loadCaptcha();
        })
        .error(function(data, status, headers, config){
          console.log(data);
          if(status === 403){
	          $scope.infomessages = "Sorry, but U R banned";
            if($scope.result_timer) clearTimeout($scope.result_timer);
            $scope.result_timer = setTimeout(function() {
              $scope.result_timer = null;
              $scope.infomessages = '';
            },10000);
          }
          $scope.loadCaptcha();
        });
      };

      $scope.loadCaptcha();
      $scope.loadChatMessages();
    }]
  }
});

app.directive('twitterfeed', function(){
  return {
    restrict: 'E',
    replace: true,
    templateUrl: '/templates/twitterfeed.html',
    controller: ['$scope', 'settings', function($scope, settings){
      $scope.widgetId = settings.twitterWidgetId;
    }]
  }
});


app.directive('navbar', function(){
  return {
    restrict: 'E',
    replace: true,
    scope: {},
    templateUrl: '/templates/navbar.html',
    controller: ['$scope', '$http', 'settings', function($scope, $http, settings){
            $scope.logout = function(){
                $http.post("/api/logout")
                .success(function(data, status, headers, config){
                    window.location = "/login";
                });
            };
            $scope.title = settings.title;
    }]
  }
});


app.directive('addinfo', function(){
  return {
    restrict: 'E',
    replace: true,
    templateUrl: '/templates/info.html'
  }
});
