var app = angular.module('radioStation', ['ngSanitize', 'twitter.timeline', 'luegg.directives']);

app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.transformRequest = function(data){
        if (data === undefined) {
            return data;
        }
        return $.param(data);
    };
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
}]);

app.filter('toHMS', function() {
  return function(input) {
      var seconds = input % 60;
      var minutes = parseInt(input / 60) % 60;
      var hours = parseInt(input / 3600) % 24;
      var data = (hours < 10 ? "0" + hours : hours) + ":" + (minutes  < 10 ? "0" + minutes : minutes) + ":" + (seconds < 10 ? "0" + seconds : seconds);
      return data;
  };
});

app.factory('audio', ['$document', function($document) {
  var audio = $document[0].createElement('audio');
  return audio;
}]);

app.factory('UserWsService', ['$rootScope', '$interval', function($rootScope, $interval){

    var listeners = [];

    var ws = new ReconnectingWebSocket('ws://' + window.location.host + '/ws');


    var pingTimer;

    ws.onopen = function(){
        console.log("Connection established");
        pingTimer = $interval(function(){
            ws.send("ping");
        }, 5000);
    }

    ws.onmessage = function(event){
        var data = JSON.parse(event.data);
        listeners.forEach(function(listener){
            //safeApply(function(){
            $rootScope.$apply(function(){
                listener(data);
            });

            //});
        });
    };


    ws.onclose = function(){
        $interval.cancel(pingTimer);
        //TODO: make reconnect by timeout
    };

    var Service = {};

    Service.register = function(listener){
        listeners.push(listener);
    };
    return Service;
}]);


app.factory('AdminWsService', ['$rootScope', '$interval', function($rootScope, $interval){
    var listeners = [];

    var ws = new ReconnectingWebSocket('ws://' + window.location.host + '/admin/ws');

    var pingTimer;

    ws.onopen = function(){
        console.log("Connection established");
        pingTimer = $interval(function(){
            ws.send("ping");
        }, 5000);
    };

    ws.onclose = function(){
        $interval.cancel(pingTimer);
        //TODO: make reconnect by timeout
    };

    ws.onmessage = function(event){
        var data = JSON.parse(event.data);
        listeners.forEach(function(listener){
            $rootScope.$apply(function(){
                listener(data);
            });
        });
    };

    var Service = {};

    Service.register = function(listener){
        listeners.push(listener);
    };
    return Service;
}]);
