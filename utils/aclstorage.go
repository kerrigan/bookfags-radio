package utils

import (
	"encoding/json"
	"io/ioutil"
	"sync"
)

type ACLStorage struct {
	bannedUsers []string
	bannedIPs   []string
	UsersBus    chan []string
	IPsBus      chan []string
	lock        sync.Locker
}

var aclStorageInstance *ACLStorage = nil

func NewACLStorage() *ACLStorage {
	if aclStorageInstance == nil {
		aclStorageInstance = &ACLStorage{
			bannedUsers: make([]string, 0),
			bannedIPs:   make([]string, 0),
			UsersBus:    make(chan []string, 1024),
			IPsBus:      make(chan []string, 1024),
			lock:        &sync.Mutex{},
		}
	}

	aclStorageInstance.loadBannedIPs()
	aclStorageInstance.loadBannedUsers()

	return aclStorageInstance
}

func (p *ACLStorage) BanUser(username string) {
	for _, v := range p.bannedUsers {
		if v == username {
			return
		}
	}

	p.lock.Lock()
	p.bannedUsers = append(p.bannedUsers, username)
	p.lock.Unlock()
	p.saveBannedUsers()
	p.UsersBus <- p.bannedUsers
}

func (p *ACLStorage) BanIP(IP string) {
	for _, v := range p.bannedIPs {
		if v == IP {
			return
		}
	}

	p.lock.Lock()
	p.bannedIPs = append(p.bannedIPs, IP)
	p.lock.Unlock()
	p.saveBannedIPs()
	p.IPsBus <- p.bannedIPs
}

func (p *ACLStorage) UnbanUser(username string) {
	i := -1

	for index, v := range p.bannedUsers {
		if v == username {
			i = index
			break
		}
	}

	if i != -1 {
		p.lock.Lock()
		p.bannedUsers = append(p.bannedUsers[:i], p.bannedUsers[i+1:]...)
		p.lock.Unlock()
		p.saveBannedUsers()
		p.UsersBus <- p.bannedUsers
	}
}

func (p *ACLStorage) UnbanIP(IP string) {
	i := -1

	for index, v := range p.bannedIPs {
		if v == IP {
			i = index
			break
		}
	}

	if i != -1 {
		p.lock.Lock()
		p.bannedIPs = append(p.bannedIPs[:i], p.bannedIPs[i+1:]...)
		p.lock.Unlock()
		p.saveBannedIPs()
		p.IPsBus <- p.bannedIPs
	}
}

func (p *ACLStorage) IsBanned(username, IP string) bool {
	for _, v := range p.bannedIPs {
		if v == IP {
			return true
		}
	}

	for _, v := range p.bannedUsers {
		if v == username {
			return true
		}
	}

	return false
}

func (p ACLStorage) GetBannedUsers() []string {
	return p.bannedUsers
}

func (p ACLStorage) GetBannedIPs() []string {
	return p.bannedIPs
}

func (p ACLStorage) saveBannedUsers() {
	data, err := json.Marshal(p.bannedUsers)

	if err != nil {
		panic(err)

	}

	err = ioutil.WriteFile("data/bannedusers.json", data, 0644)
	if err != nil {
		panic(err)
	}
}

func (p ACLStorage) saveBannedIPs() {
	data, err := json.Marshal(p.bannedIPs)

	if err != nil {
		panic(err)

	}

	err = ioutil.WriteFile("data/bannedips.json", data, 0644)
	if err != nil {
		panic(err)
	}
}

func (p *ACLStorage) loadBannedUsers() {
	var bannedUsers []string
	data, err := ioutil.ReadFile("data/bannedusers.json")

	if err != nil {
		return
	}

	err = json.Unmarshal(data, &bannedUsers)

	if err != nil {
		return
	}

	p.bannedUsers = bannedUsers
}

func (p *ACLStorage) loadBannedIPs() {
	var bannedIPs []string
	data, err := ioutil.ReadFile("data/bannedips.json")

	if err != nil {
		return
	}

	err = json.Unmarshal(data, &bannedIPs)

	if err != nil {
		return
	}

	p.bannedIPs = bannedIPs
}
