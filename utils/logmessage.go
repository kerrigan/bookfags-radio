package utils

import (
	"fmt"
	"strconv"
	"strings"
	"time"
)

type LogMessage struct {
	Timestamp time.Time `json:"timestamp"`
	Date      string    `json:"date"`
	Nick      string    `json:"nick"`
	Text      string    `json:"text"`
	IsHearer  bool      `json:"isHearer"`
	IP        string    `json:"ip"`
}

type HearerMessage struct {
	Timestamp time.Time
	Nick      string
	Text      string
	IP        string
}

func (p *HearerMessage) ColorIP() string {
	parts := strings.SplitN(p.IP, ".", 4)

	result := ""

	for _, part := range parts {
		i, err := strconv.Atoi(part)

		if err == nil {
			i = i % 216
			result += fmt.Sprintf(`<span style="background-color:%s;color:%s;">a</span>`, WEB_SAFE_COLORS[i], WEB_SAFE_COLORS[i])
		}
	}

	return result
}
