package utils

import (
	"encoding/json"
	"io/ioutil"
	"strings"
	"time"
)

type LogStorage struct {
	Messages         []LogMessage
	MessageBus       chan LogMessage
	HearerMessageBus chan HearerMessage
	HearerMessages   []HearerMessage
	Users            []string
	UsersBus         chan []string
}

var instance *LogStorage = nil

func ReplaceUrls(html string) string {
	return strings.Replace(html, "<a href", "<a target='blank' href", 1)
}

func NewLogStorage() *LogStorage {
	if instance == nil {
		instance = &LogStorage{
			Messages:         make([]LogMessage, 0),
			MessageBus:       make(chan LogMessage, 1024),
			HearerMessages:   make([]HearerMessage, 0),
			HearerMessageBus: make(chan HearerMessage, 1024),
			Users:            make([]string, 0),
			UsersBus:         make(chan []string, 1024),
		}
		instance.loadLogs()
	}

	return instance
}

func (p *LogStorage) AddMessage(nick, text string, isHearer bool, IP string) {
	if len(p.Messages) == 20 {
		p.Messages = p.Messages[1:]
	}
	now := time.Now()
	msg := LogMessage{now, now.Format("15:04:05"), nick, ReplaceUrls(text), isHearer, IP}
	p.Messages = append(p.Messages, msg)
	p.saveLogs()
	p.MessageBus <- msg
}

func (p *LogStorage) AddHearerMessage(nick, text, IP string) {
	if len(p.HearerMessages) == 20 {
		p.HearerMessages = p.HearerMessages[1:]
	}
	msg := HearerMessage{time.Now(), nick, text, IP}
	p.HearerMessages = append(p.HearerMessages, msg)
	p.HearerMessageBus <- msg
	p.AddMessage(nick, text, true, IP)
}

func (p *LogStorage) ChangeUsers(users []string) {
	p.Users = users
	p.UsersBus <- p.Users
}

func (p *LogStorage) saveLogs() {
	data, err := json.Marshal(p.Messages)

	if err != nil {
		panic(err)

	}

	err = ioutil.WriteFile("data/logs.json", data, 0644)
	if err != nil {
		panic(err)
	}
}

func (p *LogStorage) loadLogs() {
	var logs []LogMessage
	data, err := ioutil.ReadFile("data/logs.json")

	if err != nil {
		return
	}

	err = json.Unmarshal(data, &logs)

	if err != nil {
		return
	}

	p.Messages = logs
}
