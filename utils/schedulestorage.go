package utils

import (
	"encoding/json"
	"io/ioutil"
	"sort"
	"sync"
	"time"
)

type Schedule struct {
	begin time.Time
	end   time.Time

	BeginDate string `json:"beginDate"`
	BeginTime string `json:"beginTime"`
	EndDate   string `json:"endDate"`
	EndTime   string `json:"endTime"`

	Title string `json:"title"`
	Dj    string `json:"dj"`

	Finished bool `json:"finished"`
}

type Schedules []Schedule

func (v Schedules) Len() int {
	return len(v)
}

func (v Schedules) Swap(i, j int) {
	v[i], v[j] = v[j], v[i]
}

// default comparison.
func (v Schedules) Less(i, j int) bool {
	return v[i].begin.Before(v[j].begin)
}

type ScheduleStorage struct {
	Schedules   Schedules
	ScheduleBus chan []Schedule
	lock        sync.Locker
}

func NewScheduleStorage() *ScheduleStorage {
	instance := &ScheduleStorage{
		Schedules:   make([]Schedule, 0),
		ScheduleBus: make(chan []Schedule, 1024),
		lock:        &sync.Mutex{},
	}

	instance.loadSchedules()

	return instance
}

func (p *ScheduleStorage) loadSchedules() {
	var schedules []Schedule
	data, err := ioutil.ReadFile("data/schedules.json")

	if err != nil {
		return
	}

	err = json.Unmarshal(data, &schedules)

	if err != nil {
		return
	}

	tsFormat := "02.01.2006 15:04:05"

	for i, schedule := range schedules {
		beginTS, err := time.Parse(tsFormat, schedule.BeginDate+" "+schedule.BeginTime)
		if err != nil {
			return
		}

		endTS, err := time.Parse(tsFormat, schedule.EndDate+" "+schedule.EndTime)
		if err != nil {
			return
		}

		schedules[i].begin = beginTS
		schedules[i].end = endTS
	}

	p.Schedules = schedules
}

func (p *ScheduleStorage) GetSchedules() Schedules {
	for i := range p.Schedules {
		if p.Schedules[i].begin.Before(time.Now()) {
			p.Schedules[i].Finished = true
		} else {
			p.Schedules[i].Finished = false
		}
	}
	return p.Schedules
}

func (p *ScheduleStorage) saveSchedules() {
	for i := range p.Schedules {
		if p.Schedules[i].begin.Before(time.Now()) {
			p.Schedules[i].Finished = true
		} else {
			p.Schedules[i].Finished = false
		}
	}

	data, err := json.Marshal(p.Schedules)

	if err != nil {
		panic(err)
	}

	err = ioutil.WriteFile("data/schedules.json", data, 0644)
	if err != nil {
		panic(err)
	}
}

func (p *ScheduleStorage) AddSchedule(begin, end time.Time, title, dj string) {
	//build from datetime
	p.lock.Lock()
	p.Schedules = append(
		p.Schedules,
		Schedule{
			begin:     begin,
			end:       end,
			Title:     title,
			Dj:        dj,
			BeginDate: begin.Format("02.01.2006"),
			BeginTime: begin.Format("15:04:05"),
			EndDate:   end.Format("02.01.2006"),
			EndTime:   end.Format("15:04:05"),
		},
	)
	//and sort by date
	sort.Sort(p.Schedules)
	p.lock.Unlock()

	p.saveSchedules()
	p.ScheduleBus <- p.Schedules
}

func (p *ScheduleStorage) RemoveSchedule(index int) {
	if index >= len(p.Schedules) {
		return
	}

	p.lock.Lock()
	p.Schedules = append(p.Schedules[:index], p.Schedules[index+1:]...)
	p.lock.Unlock()
	p.saveSchedules()
	p.ScheduleBus <- p.Schedules
}

func (p *ScheduleStorage) EditSchedule(index int, begin, end time.Time, title, dj string) {
	if index >= len(p.Schedules) {
		return
	}

	p.lock.Lock()
	p.Schedules[index] = Schedule{
		begin:     begin,
		end:       end,
		Title:     title,
		Dj:        dj,
		BeginDate: begin.Format("02.01.2006"),
		BeginTime: begin.Format("15:04:05"),
		EndDate:   end.Format("02.01.2006"),
		EndTime:   end.Format("15:04:05"),
	}
	sort.Sort(p.Schedules)
	p.lock.Unlock()
	p.saveSchedules()
	p.ScheduleBus <- p.Schedules
}
